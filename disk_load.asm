disk_load:
    push dx

    mov ah, 0x02                ; establish read int code
    mov al, dh
    mov ch, 0x00
    mov dh, 0x00
    mov cl, 0x02                ; necessary to skip boot sector (1 ind)

    int 0x13

    jc disk_error_flag          ; jump if carry flag set

    pop dx
    cmp dh, al
    jne disk_error_sect              ; dh contains sectors expected, al contains sectors read

    ret

disk_error_flag:
    mov bx, disk_error_flag_msg
    call print
    mov dl, ah
    call print_hex
    jmp $

disk_error_sect:
    mov bx, disk_error_sect_msg
    call print
    jmp $

%include "print.asm"
disk_error_flag_msg db "ERROR flag!", 0
disk_error_sect_msg db "ERROR sect!", 0
