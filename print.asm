%ifndef PRINT
    %define PRINT                   ; basically, only define it if not already defined
        print:                      ; function entry point
            pusha
            print_start:            ; function beginning
                mov ax, [bx]        ; set ax to byte at arg reference (bx is arg)
                inc bx              ; move on to next byte of arg
                cmp al, 0           ; if byte is 0...
                je print_finish     ; end the printing
                mov ah, 0x0e        ; otherwise, prepare to print
                int 0x10            ; print interrupt
                jmp print_start     ; go back to the beginning
            print_finish:
                mov ah, 0x0e        ; print the separator
                mov al, ' '
                int 0x10
                popa                ; restore state and go back
                ret
%endif
