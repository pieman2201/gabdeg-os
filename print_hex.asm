print_hex:
    pusha
    mov bx, hexstr                      ; init string into bx
    add bx, 2

    mov cx, 16                          ; set highest shift

    print_hex_start:

        sub cx, 4                       ; reduce shift to iterate through digits

        mov ax, dx                      ; copy arg
        shr ax, cl                      ; shift arg back
        and ax, 0xF                     ; take lowest digit of arg
        call hex_to_char
        mov [bx], ax                    ; replace digit in base str with new thing
        inc bx                          ; step to next char in base

        cmp cx, 0                       ; end if reached the lowest digit
        jne print_hex_start

    mov bx, hexstr
    call print
    popa
    ret

hex_to_char:
    cmp ax, 10                          ; converts hex number to ascii representation
    jl hex_not_a_number
    add ax, 7                           ; offset required due to gap between numbers and letters
    hex_not_a_number:
    add ax, 48
    ret

%include "print.asm"
hexstr db '0x0000', 0
