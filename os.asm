[org 0x7c00]
    mov bx, start_msg
    call print
    mov [boot_drive], dl

    mov bp, 0x8000                  ; establish stack
    mov sp, bp

    mov bx, 0x9000
    mov dh, 2                       ; read 2 sectors (there are only 2 after the boot sector)
    mov dl, [boot_drive]
    call disk_load

    mov dx, [0x9000]                  ; change location to our output
    call print_hex

    mov dx, [0x9000 + 512]          ; next section of output (after 256*2 bytes)
    call print_hex

    jmp $
%include "disk_load.asm"
%include "print_hex.asm"
%include "print.asm"

boot_drive db 0

start_msg db "hello!", 0

times 510-($-$$) db 0               ; padding for boot sector
dw 0xaa55                           ; magicks

times 256 dw 0xdada                 ; this is our test data
times 256 dw 0xface
